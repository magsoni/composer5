
;■条件
;・キャラクターはあかね、やまとを使用
;・やまとの表情はnormal、angry使用不可
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・真剣な表情がない中でどう演出するか
;・後半の靴の話でどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;窶披披披披披披披蝿ﾈ下よりスクリプトを組んでください窶披披披披披披披髏

@layopt layer=message0 visible=true

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="キャラ１"]
;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/normal.png" jname="キャラ２"]
;キャラ１表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="キャラ１"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="キャラ１"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="キャラ１"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="キャラ１"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="キャラ１"]

;キャラ２表情
[chara_face name="yamato" face="happy" storage="chara/yamato/happy.png" jname="キャラ２"]
[chara_face name="yamato" face="sad" storage="chara/yamato/sad.png" jname="キャラ２"]
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="キャラ２"]
[chara_face name="yamato" face="back" storage="chara/yamato/back.png" jname="キャラ２"]
;背景
[bg storage="gaikan_hiru.jpg" ]
;メニューボタン
@showmenubutton

[chara_mod name="yamato"  face="back"]
[chara_show name="yamato" left=200 top=0 ]
[wait time=800]

[chara_hide name="yamato"]

[chara_mod name="akane"  face="doki"]
[chara_show name="akane" left=480 top=50 ]


[chara_mod name="yamato"  face="back"]
[chara_show name="yamato" left=50 top=0]


#キャラ１ 
キャラ２がすごく真剣な顔をしてる……。[r]
何あったの！？[l]

[cm]

#キャラ２ 
SNSにどの写真載せようか悩んでるんだよ……[l]

[cm]

[chara_mod name="akane"  face="happy"]

#キャラ１ 
楽しそうなの載せたら良いと思うな！[l]

[cm]

[chara_mod name="yamato"  face="happy"]

#キャラ２ 
じゃあ一緒に撮ろうぜ！[l]

[cm]

[chara_mod name="akane"  face="normal"]

#キャラ１ 
でも、ただ載せるんじゃ面白くないよ？[l]

[cm]

[chara_mod name="yamato"  face="tohoho"]

#キャラ２ 
じゃあ……[l]

[cm]

[chara_mod name="akane"  face="happy"]
[wait time=100]

[anim name=akane top="-=10" time=100]
[wait time=100]
[anim name=akane top="+=10" time=100]
#キャラ１ 
あ！　キャラ２新しい靴履いてたよね！[r]
私も買ったばかりだから、靴履いて写真撮ってみない？[l]

[cm]

[mask time=400]
[chara_mod name="akane"  face="normal"]
[chara_mod name="yamato"  face="happy"]
[mask_off time=200]
[wait time=200]


#キャラ２ 
じゃあ、撮るぜ！[l]

[cm]

[chara_mod name="akane"  face="angry"]

#キャラ１ 
ちょっと待って！[r]
やっぱり、もう少し靴を近くした方がよくない？[l]

[cm]

[chara_mod name="akane"  face="normal"]
[chara_mod name="yamato"  face="back"]
[anim name=yamato left="+=50" time=500 effect="easeOutCirc"]
[anim name=akane left=" -=40" time=600 ]

#キャラ２ 
こんな感じか？[l]

[cm]

[chara_mod name="akane"  face="happy"]
[chara_mod name="yamato"  face="happy"]
#キャラ２ 
なら……はい、チーズ[l]

[cm]

[mask time=100]


[mask_off time=200]
[wait time=1500]


[chara_mod name="akane"  face="normal"]

#キャラ１ 
いい感じの写真撮れた？[l]

[cm]

[chara_mod name="yamato"  face="happy"]

#キャラ２ 
これならSNSに載せられそうだな！[l]

[mask]
@layopt layer=message0 visible=false
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]

 
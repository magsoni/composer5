*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

@layopt layer=message0 visible=true

;あかね
[chara_new  name="ch1" storage="chara/akane/normal2.png" jname="あかね"]
;あかね鏡
[chara_new  name="ch4" storage="chara/akane/normal2.png" jname="あかね"]
;やまと
[chara_new  name="ch2" storage="chara/yamato/normal2.png" jname="やまと"]
;やまと鏡
[chara_new  name="ch5" storage="chara/yamato/normal2.png" jname="やまと"]


;あかね表情
[chara_face name="ch1" face="normal" storage="chara/akane/normal2.png"]
[chara_face name="ch1" face="angry" storage="chara/akane/angry2.png"]
[chara_face name="ch1" face="doki" storage="chara/akane/doki2.png"]
[chara_face name="ch1" face="happy" storage="chara/akane/happy2.png"]
[chara_face name="ch1" face="sad" storage="chara/akane/sad2.png"]
[chara_face name="ch1" face="back" storage="chara/akane/back.png"]

;あかね鏡表情
[chara_face name="ch4" face="normal" storage="chara/akane/normal2.png"]
[chara_face name="ch4" face="angry" storage="chara/akane/angry2.png"]
[chara_face name="ch4" face="doki" storage="chara/akane/doki2.png"]
[chara_face name="ch4" face="happy" storage="chara/akane/happy2.png"]
[chara_face name="ch4" face="sad" storage="chara/akane/sad2.png"]
[chara_face name="ch4" face="back" storage="chara/akane/back.png"]

;やまと表情
[chara_face name="ch2" face="normal" storage="chara/yamato/normal2.png"]
[chara_face name="ch2" face="angry" storage="chara/yamato/angry2.png"]
[chara_face name="ch2" face="tohoho" storage="chara/yamato/tohoho2.png"]
[chara_face name="ch2" face="happy" storage="chara/yamato/happy2.png"]
[chara_face name="ch2" face="sad" storage="chara/yamato/sad2.png"]
[chara_face name="ch2" face="back" storage="chara/yamato/back.png"]

;やまと鏡表情
[chara_face name="ch5" face="normal" storage="chara/yamato/normal2.png"]
[chara_face name="ch5" face="angry" storage="chara/yamato/angry2.png"]
[chara_face name="ch5" face="tohoho" storage="chara/yamato/tohoho2.png"]
[chara_face name="ch5" face="happy" storage="chara/yamato/happy2.png"]
[chara_face name="ch5" face="sad" storage="chara/yamato/sad2.png"]
[chara_face name="ch5" face="back" storage="chara/yamato/back.png"]

;----keyframeの定義
[keyframe name="kh"]

[frame p=100% rotate="45deg" ]

[endkeyframe]

;-----定義したアニメーションを実行

;背景
[image layer="0" folder="bgimage" name="seitokai2" storage="seitokai_yu2.jpg" visible="true" top="0" left="0" width="1200" height="675"]

@showmenubutton

[chara_mod  name="ch4" face="happy"]
[chara_show  name="ch4" left=310 top=60 layer="1" reflect="true"]

#あかね
鏡よ、鏡よ、鏡さん[p]

[chara_mod  name="ch4" face="normal"]

#あかね
世界で一番美しいのはだーれ？[p]

[mask time=1000]
[chara_hide name="ch4" time="0" layer="1"]
[chara_mod  name="ch4" face="normal" time="0"]
[chara_show  name="ch4" left=380 top=60 layer="1" reflect="true" time="0"]
[chara_mod  name="ch1" face="back" time="0"]
[chara_mod  name="ch2" face="back" time="0"]
[chara_show  name="ch1" left=550 top=75 layer="1" time="0"]
[chara_show  name="ch5" left=-10 top=30 reflect="true" time="0"]
[chara_show  name="ch2" left=-50 top=35 layer="1" time="0"]
[filter layer="1" name="ch4" invert=5 time="0"]
[filter layer="0" name="ch5" invert=5 time="0"]
[image layer="0" folder="bgimage" name="siro" storage="siro.jpg" visible="true" top="-200" left="300" width="40" height="400" time="0"]
[image layer="0" folder="bgimage" name="siro2" storage="siro.jpg" visible="true" top="-180" left="300" width="10" height="500" time="0"]
[anim name="siro" time=0  opacity=100]
[anim name="siro2" time=0  opacity=100]
[kanim name="siro" keyframe="kh" time=0]
[kanim name="siro2" keyframe="kh" time=0]
[wa]
[image layer="0" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="0" left="700" width="500" height="675" time="0"]
[image layer="0" folder="bgimage" name="kuro" storage="kuro.jpg" visible="true" top="0" left="-250" width="500" height="675" time="0"]
[mask_off time=1000]

#やまと
……おまえ、いったい何やってるんだ？[p]

[chara_mod  name="ch4" face="doki"]

[anim name=ch4 top="-=10" time=100]
[anim name=ch1 top="-=10" time=100]
[wait time=100]
[anim name=ch4 top="+=10" time=100]
[anim name=ch1 top="+=10" time=100]
[wait time=100]

#あかね
や、やまと！？[p]

[chara_mod  name="ch5" face="tohoho"]

#やまと
あまりにも真剣にやってるから、声かけ辛くてな……[p]

[anim name=ch4 left="-=5" time=150]
[anim name=ch1 left="-=5" time=150]
[wait time=150]
[anim name=ch4 left="+=10" time=300]
[anim name=ch1 left="+=10" time=300]
[wait time=300]
[anim name=ch4 left="-=5" time=150]
[anim name=ch1 left="-=5" time=150]
[wait time=150]

#あかね
あはは……[p]

[mask time=500]

[chara_hide name="ch4" layer="1" time="0"]
[chara_hide name="ch5" time="0"]
[chara_hide name="ch1" layer="1" time="0"]
[chara_hide name="ch2" layer="1" time="0"]

[chara_mod  name="ch1" face="normal" time="0"]
[chara_mod  name="ch2" face="normal" time="0"]

[image layer="0" folder="bgimage" name="seitokai" storage="seitokai_yu.jpg" visible="true" top="0" left="0" width="1200" height="675" time="0"]

[chara_show  name="ch1" left=100 top=55 time="0"]
[chara_show  name="ch2" left=280 top=15 time="0"]

[mask_off time=1000]

#あかね
実は、演劇の練習をしていたんだ！[p]

@layopt layer=message0 visible=false

[chara_mod  name="ch4" face="happy"]
[chara_mod  name="ch1" face="happy"]
[anim name=ch1 top="-=10" time=100]
[wait time=100]
[anim name=ch1 top="+=10" time=100]
[wait time=100]

@layopt layer=message0 visible=true

#あかね
あ！　やまとも練習手伝ってよ！　これ台本ね！[p]

[chara_mod  name="ch2" face="tohoho"]
[anim name=ch2 left="+=10" time=150]
[wait time=150]

#やまと
まだやるって言ってないんだが……[p]

[chara_mod  name="ch4" face="normal"]
[anim name=ch4 left="+=20" effect="easeInOutQuint" time=300]

#あかね
まあまあ、そんなこと言わずに……[p]

[chara_mod  name="ch4" face="happy"]

#あかね
あとでアイス馳走するから！[p]

[chara_mod  name="ch2" face="happy"]
[anim name=ch2 left="-=10" time=50]
[wait time=50]

#やまと
やらせていただきます！[p]

[anim name=ch1 top="-=10" time=100]
[wait time=100]
[anim name=ch1 top="+=10" time=100]
[wait time=100]

#あかね
本当に！　やった！[p]

[chara_mod  name="ch1" face="normal"]

#あかね
よーし！　練習再開だ！[p]

[mask]
@layopt layer=message0 visible=false
@hidemenubutton
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]

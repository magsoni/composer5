
;■条件
;・キャラクターはあかね、やまとを使用
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・鏡を見ている際にどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

@layopt layer=message0 visible=true

;————————以下よりスクリプトを組んでください————————

;あかね定義

[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

[chara_new name="akane_Mirror" storage="chara/akane/normal.png" reflect=true]

;やまと定義

[chara_new name="yamato" storage="chara/yamato/normal.png" jname="やまと"]

;あおい定義

[chara_new name="aoi" storage="chara/aoi/normal.png" jname="あおい"]


;あかね表情

[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]

[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]

[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]

[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]

[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]



[chara_face name="akane_Mirror" face="happy" storage="chara/akane/happy.png" jname="あかね"]

[chara_face name="akane_Mirror" face="doki" storage="chara/akane/doki.png" jname="あかね"]

[chara_face name="akane_Mirror" face="sad" storage="chara/akane/sad.png" jname="あかね"]

[chara_face name="akane_Mirror" face="angry" storage="chara/akane/angry.png" jname="あかね"]


;やまと表情

[chara_face name="yamato" face="normal" storage="chara/yamato/normal.png" jname="やまと"]

[chara_face name="yamato" face="sad" storage="chara/yamato/sad.png" jname="やまと"]

[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="やまと"]

[chara_face name="yamato" face="angry" storage="chara/yamato/angry.png" jname="やまと"]

[chara_face name="yamato" face="happy" storage="chara/yamato/happy.png" jname="やまと"]

;あおい表情
[chara_face name="aoi" face="normal" storage="chara/aoi/normal.png" jname="あおい"]

[chara_face name="aoi" face="happy" storage="chara/aoi/happy.png" jname="あおい"]

[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png" jname="あおい"]

[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png" jname="あおい"]

[chara_face name="aoi" face="angry" storage="chara/aoi/angry.png" jname="あおい"]

;背景

[bg storage="aki_kyositu.jpg" time="1500"]

;メニューボタン
@showmenubutton

[chara_mod name="akane" wait=false face="sad"]

[chara_mod name="akane_Mirror" wait=false face="sad"]

[playbgm storage="sad.ogg"]

[chara_show name="akane_Mirror" wait=false reflect=true left=50 top=50]

[chara_show name="akane" wait=false left=450 top=50]

[chara_show name="akane" left=280 top=50]



#あかね
鏡よ、鏡よ、鏡さん[l]

[cm]

[chara_mod name="akane" wait=false face="angry"]

[chara_mod name="akane_Mirror" wait=false face="angry"]


#あかね
世界で一番美しいのはだーれ？[l]


[cm]

@layopt layer=message0 visible=false


[wait time=1000]

[chara_hide_all]

[stopbgm]

[playse storage="class_door1.ogg"]

[chara_mod name="yamato" face="tohoho"]

[chara_show name="yamato" left=600 top=20]

[wait time=1000]

[playbgm storage="dozikkomarch.ogg"]

@layopt layer=message0 visible=true

#やまと
……おまえ、いったい何やってるんだ？？[l]


[cm]

[chara_hide_all]

[chara_mod name="akane" wait=false face="doki"]

[chara_mod name="akane_Mirror" wait=false face="doki"]

[chara_show name="akane" wait=false left=450 top=50]

[chara_show name="akane_Mirror" wait=false reflect=true left=50 top=50]



#あかね
や、やまと！？[l]

@layopt layer=message0 visible=false
[cm]

[chara_hide_all]

[mask time=500]

[wait time=500]

[playbgm storage="normal.ogg"]

[mask_off time=500]

[chara_mod name="akane" face="normal"]

[chara_mod name="yamato" face="normal"]

[chara_show name="akane" wait=false left=50 top=50]

[chara_show name="yamato" wait=false left=450 top=20]

@layopt layer=message0 visible=true

#やまと
あまりにも真剣にやってるから、ちょっと声かけ辛くてな……[l]

[cm]

[chara_mod name="akane" face="happy"]

#あかね
あはは……[l]

[cm]

#あかね
実は、演劇の練習をしていたんだ！[l]

[cm]

[chara_hide_all]

[chara_mod name="akane" face="happy"]

[chara_show name="aoi" wait=false left=50 top=50]

[chara_show name="akane" wait=false left=450 top=50]


#あかね
あ！　あおいちゃんも練習手伝ってよ！　これ台本ね！[l]

[cm]

[chara_hide_all]

[chara_mod name="akane" face="normal"]

[chara_mod name="yamato" face="tohoho"]

[chara_show name="akane" wait=false left=50 top=50]

[chara_show name="yamato" wait=false left=450 top=20]


#やまと
まだやるって言ってないんだが……[l]

[cm]

[anim name="akane" left="+=80" time=300 effect="easeInSine"]

#あかね
まあまあ、そんなこと言わずに……[l]

[cm]

[chara_mod name="akane" face="happy"]


#あかね
あとでアイス御馳走するから！[l]

[cm]

[chara_mod name="yamato" face="happy"]


[anim name="yamato" left="-=50" time=150 effect="easeInSine"]

#やまと
やらせていただきます！[l]

[cm]


[anim name="akane" top="-=30" time=100 effect="easeInSine"]

[wa]

[anim name="akane" top="+=30" time=100 effect="easeInSine"]

#あかね
本当に！　やった！[l]

[cm]

#あかね
よーし！　練習再開だ！[l]

[cm]

@layopt layer=message0 visible=false

[mask]

[stopbgm]

[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
[mask_off]
[jump first.ks]



*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;あかね
[chara_new  name="ch1" storage="chara/akane/normal2.png" jname="あかね"]

;ひぃ
[chara_new  name="ch3" storage="chara/hi/hi01.png" jname="ひぃ"]

;あかね表情
[chara_face name="ch1" face="normal" storage="chara/akane/normal2.png"]
[chara_face name="ch1" face="angry" storage="chara/akane/angry2.png"]
[chara_face name="ch1" face="doki" storage="chara/akane/doki2.png"]
[chara_face name="ch1" face="happy" storage="chara/akane/happy2.png"]
[chara_face name="ch1" face="sad" storage="chara/akane/sad2.png"]
[chara_face name="ch1" face="back" storage="chara/akane/back.png"]

;ひぃ表情
[chara_face name="ch3" face="normal" storage="chara/hi/hi01.png"]
[chara_face name="ch3" face="angry" storage="chara/hi/hi06.png"]
[chara_face name="ch3" face="surprise" storage="chara/hi/hi04.png"]
[chara_face name="ch3" face="happy" storage="chara/hi/hi03.png"]
[chara_face name="ch3" face="sad" storage="chara/hi/hi02.png"]

;背景
[image layer="0" folder="bgimage" name="gaikan" storage="gaikan_hiru.jpg" visible="true" top="0" left="0" width="960" height="720" time="100"]

@showmenubutton

[chara_mod  name="ch3" face="normal"]
[chara_show  name="ch3" layer="1" width="360" height="540" left=200 top=110]

[chara_show  name="ch1"  left=500 top=60 time=0 opacity=0]
[anim name="ch1" time=0  opacity=0]
[anim name=ch1 left="-=120" effect="easeInQuint" time=600 opacity=255]
[wait time=600]

[anim name=ch1 left="-=10" top="+=5" effect="easeOutQuint" time=500]
[anim name=ch3 left="-=5" top="+=3" effect="easeOutQuint" time=300]
[wait time=500]

@layopt layer=message0 visible=true

#あかね
ひぃちゃーん！[p]

[chara_mod  name="ch3" face="surprise"]

#ひぃ
あかね！？[p]

[chara_mod  name="ch3" face="angry"]

#ひぃ
急に抱き着かないでよ！[p]

[chara_mod  name="ch1" face="angry"]

#あかね
えー！　いいじゃん[p]

[chara_mod  name="ch1" face="normal"]

#あかね
ところで、ひぃちゃんは何やってるの？[p]

[chara_mod  name="ch3" face="normal"]

#ひぃ
はぁ……、珍しい鳥がいたから見てたのよ[p]

@layopt layer=message0 visible=false 

[chara_hide name="ch1" time=0]
[chara_hide name="ch3" layer="1" time=0]
[wait time=200]

[camera x=0 y=0 zoom=1.5 time=500]

[anim name=gaikan left="-=100" effect="easeOutQuint" time=500]
[wait time=500]
[anim name=gaikan left="+=200" effect="easeOutQuint" time=1000]
[wait time=1000]
[anim name=gaikan left="-=100" effect="easeInQuint" time=500]
[wait time=500]

@layopt layer=message0 visible=true

#あかね
え！　どこどこ！？[p]

@layopt layer=message0 visible=false

[camera x=0 y=0 zoom=1 time=1000]

[chara_mod  name="ch1" face="happy" time=0]
[chara_show  name="ch3" width="360" height="540" layer="1" left=200 top=110 time=200 wait=false]
[chara_show  name="ch1"  left=380 top=65 time=200 wait=true]
[wait time=200]

[anim name=ch1 top="-=10" time=100]
[wait time=100]
[anim name=ch1 top="+=10" time=100]
[wait time=100]

@layopt layer=message0 visible=true

#あかね
あ！　そういえば、[r]これから新しく出来たカフェ行くんだけど、一緒に行かない？[p]

#ひぃ
もう少し鳥を見たいから遠慮しとくわ[p]

[chara_mod  name="ch1" face="sad"]

[anim name=ch1 left="-=10" time=100]
[anim name=ch3 left="-=5" time=100]
[wait time=100]
[anim name=ch1 left="+=10" time=100]
[anim name=ch3 left="+=5" time=100]
[wait time=100]
[anim name=ch1 left="-=10" time=100]
[anim name=ch3 left="-=5" time=100]
[wait time=100]
[anim name=ch1 left="+=10" time=100]
[anim name=ch3 left="+=5" time=100]
[wait time=100]

#あかね
えー！　行こうよー[p]

[chara_mod  name="ch3" face="angry"]

#ひぃ
ちょっと人前で恥ずかしいから辞めなさい！[p]

@layopt layer=message0 visible=false

[camera x=-100 y=40 zoom=1.7 time=500]
[anim name=ch3 top="-=10" time=100]
[wait time=100]
[anim name=ch3 top="+=10" time=100]
[wait time=100]

@layopt layer=message0 visible=true

#ひぃ
と言うより……いつまで抱き着いてるのよ！[p]

[mask]
@layopt layer=message0 visible=false
@hidemenubutton
[chara_hide_all]
[freeimage layer="base"]
[mask_off]
[jump first.ks]

 
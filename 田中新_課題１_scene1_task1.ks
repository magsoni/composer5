
;■条件
;・使用キャラクターは自由
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・急に話が飛んでいる場合に、場所などをユーザーが理解できるようにスクリプトが組めるか
;・キャラ性を理解できているか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;————————以下よりスクリプトを組んでください————————

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/tohoho.png" jname="やまと"]

;キャラ１表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]

;キャラ２表情
[chara_face name="yamato" face="normal" storage="chara/yamato/normal.png" jname="やまと"]
[chara_face name="yamato" face="angry" storage="chara/yamato/angry.png" jname="やまと"]
[chara_face name="yamato" face="happy" storage="chara/yamato/happy.png" jname="やまと"]
[chara_face name="yamato" face="sad" storage="chara/yamato/sad.png" jname="やまと"]
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="やまと"]

;背景
[bg storage="tyousakamiti.jpg" time="1500"]

;メニューボタン
@showmenubutton

[chara_mod name="akane"  face="happy"]
[chara_mod name="yamato"  face="angry"]
[chara_show name="akane" left=280 top=50]
[chara_show name="yamato" left=980 top=50]
[playbgm storage="dozikkomarch.ogg"]

@layopt layer=message0 visible=true

#あかね 
やまと！[r]
早く早くー！[l]

[cm]

@layopt layer=message0 visible=false

[anim name=akane top="+=75" time=50 effect="easeInSine"]
[playse storage="jump1.ogg"]
[wait time=200]

[anim name=akane top="-=75" time=50 effect="easeInSine"]
[wait time=500]

[anim name=akane top="+=75" time=50 effect="easeInSine"]
[playse storage="jump1.ogg"]
[wait time=200]

[anim name=akane top="-=75" time=50 effect="easeInSine"]
[wait time=500]

[wa]

@layopt layer=message0 visible=true

#あかね 
そんなのんびりしてると、遅刻しちゃうぞ！[l]

[cm]

@layopt layer=message0 visible=false

[anim name=akane left="-=700" time=500 effect="easeInSine" opacity=0]
;[playse storage="landing.ogg"]
;[playse storage="walk-snow.ogg"]
[wait time=500]
[wa]

[chara_hide name="akane"]
[wait time=1000]

[anim name=yamato left="-=680" time=500 effect="easeInSine"]
[wait time=1000]

@layopt layer=message0 visible=true
#やまと
……分かってるって！[r]
この坂道きついんだよ！[l]

[cm]

@layopt layer=message0 visible=false

[anim name=yamato left="-=700" time=500 effect="easeInSine" opacity=0]
[wait time=500]
[wa]

[chara_hide name="yamato"]
[wait time=1000]

[mask time=1000]
[chara_hide_all]
[bg storage="room.jpg" time="1500"]
;[anim name=yamato left="=80" time=0]
;[anim name=akane left="-=150" time=0]
[chara_mod name="akane"  face="normal"]
[chara_mod name="yamato"  face="tohoho"]
[mask_off time=1000]

[chara_show name="akane" left=80 top=50]
@layopt layer=message0 visible=true
#あかね 
到着！[l]

[cm]

[chara_mod name="akane"  face="happy"]
#あかね 
みんな、おっはよー！[l]

[cm]

[chara_show name="yamato" left=480 top=50]
#やまと
はぁはぁ……[l]

[cm]

[chara_mod name="yamato"  face="angry"]
#やまと
置いてくんじゃねえよ……[r]
この体力馬鹿が！[l]

[cm]

[chara_mod name="akane"  face="doki"]
#あかね 
ほら！[r]
授業始まっちゃうよー[l]

[cm]

[chara_mod name="akane"  face="happy"]
#あかね 
席についてー[l]

[cm]

[chara_mod name="yamato"  face="sad"]
#やまと
はあ……、わかったよ[l]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]
 
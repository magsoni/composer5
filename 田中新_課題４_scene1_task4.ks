
;■条件
;・キャラクターはあかね、やまとを使用
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・鏡を見ている際にどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

@layopt layer=message0 visible=true

;————————以下よりスクリプトを組んでください————————

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/tohoho.png" jname="やまと"]

;キャラ１表情
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]
[chara_face name="akane" face="back" storage="chara/akane/back.png" jname="あかね"]

;キャラ２表情
[chara_face name="yamato" face="normal" storage="chara/yamato/normal.png" jname="やまと"]
[chara_face name="yamato" face="angry" storage="chara/yamato/angry.png" jname="やまと"]
[chara_face name="yamato" face="happy" storage="chara/yamato/happy.png" jname="やまと"]
[chara_face name="yamato" face="sad" storage="chara/yamato/sad.png" jname="やまと"]
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="やまと"]

;背景
[bg storage="room.jpg" time="1500"]

;メニューボタン
@showmenubutton

[playbgm storage="dozikkomarch.ogg"]

[chara_mod name="akane"  face="back"]
[chara_show name="akane" left=280 top=50]

[wait time=1000]

[anim name=akane left="+=50" time=500 effect="easeInSine"]
[wait time=500]

[anim name=akane left="-=100" time=500 effect="easeInSine"]
[wait time=500]

[anim name=akane left="+=50" time=500 effect="easeInSine"]

[wait time=1000]

[anim name=akane top="+=75" time=50 effect="easeInSine"]
[wait time=200]

[anim name=akane top="-=75" time=50 effect="easeInSine"]
[wait time=500]

[anim name=akane top="+=75" time=50 effect="easeInSine"]
[wait time=200]

[anim name=akane top="-=75" time=50 effect="easeInSine"]

[wait time=1000]

@layopt layer=message0 visible=true

#あかね
鏡よ、鏡よ、鏡さん[l]

[cm]

[chara_mod name="akane"  face="happy"]
#あかね
世界で一番美しいのはだーれ？[l]

[cm]

[chara_hide name="akane"]
[wait time=500]

[chara_mod name="yamato"  face="angry"]
[chara_show name="yamato" left=280 top=0]
#やまと
……おまえ、[r]
いったい何やってるんだ？？[l]

[cm]

[chara_hide name="yamato"]

[chara_mod name="akane"  face="doki"]
[chara_mod name="yamato"  face="tohoho"]
[chara_show name="akane" left=50 top=50]
[chara_show name="yamato" left=430 top=0]
#あかね
や、やまと！？[l]

[cm]

#やまと
あまりにも真剣にやってるから、[r]
ちょっと声かけ辛くてな……[l]

[cm]

[chara_mod name="akane"  face="sad"]
#あかね
あはは……[l]

[cm]

[chara_mod name="akane"  face="normal"]
[chara_mod name="yamato"  face="normal"]
#あかね
実は、演劇の練習をしていたんだ！[l]

[cm]

;【田中新】元シナリオ、“キャラ３ちゃん”の文言が出てくるが、登場キャラ二人なので整合性考え“やまと”表記
[chara_mod name="akane"  face="happy"]
#あかね
あ！[r]
やまとも練習手伝ってよ！[l]

[cm]

#あかね
これ台本ね！[l]

[cm]

@layopt layer=message0 visible=false

[wait time=200]
[anim name=akane left="+=120" time=100 effect="easeInSine"]
[playse storage="人にぶつかりました.ogg"]
[chara_mod name="yamato"  face="sad"]
[wait time=200]
[anim name=akane left="-=120" time=100 effect="easeInSine"]

[wait time=1000]
[wa]

@layopt layer=message0 visible=true

#やまと
まだやるって[r]
言ってないんだが……[l]

[cm]

[chara_mod name="akane"  face="normal"]
#あかね
まあまあ、[r]
そんなこと言わずに……[l]

[cm]

[chara_mod name="akane"  face="happy"]
#あかね
あとでアイス御馳走するから！[l]

[cm]

[chara_mod name="yamato"  face="happy"]
#やまと
やらせていただきます！[l]

[cm]

@layopt layer=message0 visible=false

[wait time=500]

[anim name=akane top="+=75" time=50 effect="easeInSine"]
[wait time=200]

[anim name=akane top="-=75" time=50 effect="easeInSine"]

[wait time=1000]

[wa]

@layopt layer=message0 visible=true
#あかね
本当に！
やった！[l]

[cm]

[chara_mod name="akane"  face="normal"]
#あかね
よーし！
練習再開だ！[l]

[cm]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]

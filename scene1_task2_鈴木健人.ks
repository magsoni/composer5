
;■条件
;・使用キャラクターは自由
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・抱き着く際の演出に違和感がないように
;・普通に組むと盛り上がりに欠けるので、どこかしらに演出を

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;————————以下よりスクリプトを組んでください————————

@layopt layer=message0 visible=true

;あかね定義

[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]

;あおい定義

[chara_new name="aoi" storage="chara/aoi/normal.png" jname="あおい"]

;あかね表情

[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]

[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]

[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]

[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]

[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]

;あおい　表情
[chara_face name="aoi" face="normal" storage="chara/aoi/normal.png" jname="あおい"]

[chara_face name="aoi" face="happy" storage="chara/aoi/happy.png" jname="あおい"]

[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png" jname="あおい"]

[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png" jname="あおい"]

[chara_face name="aoi" face="angry" storage="chara/aoi/angry.png" jname="あおい"]
;背景

[bg storage="room.jpg" time="1500"]


;メニューボタン
@showmenubutton

[playbgm storage="normal.ogg"]

[playse storage="suzume.ogg"]

@layopt layer=message0 visible=true

[chara_show name="aoi" left=50 top=80]

[wait time=1000]

[chara_mod name="akane"  face="happy"]

[chara_show name="akane" left=550 top=80]

[chara_mod name="akane"  face="happy"]

[wait time=500]


[anim name=akane left="-=300" time=800 effect="easeInSine"]

[chara_mod name="akane"  face="happy"]

[playse storage="人にぶつかりました.ogg"]

#あかね
あおいちゃーん！[l]



[cm]

[chara_mod name="aoi"  face="doki"]

#あおい
あかね！？[l]

[cm]

[chara_mod name="aoi"  face="angry"]

[anim name=akane left="+=50" time=800 effect="easeInSine"]

#あおい
急に抱き着かないでよ！[l]

[cm]

[chara_mod name="akane"  face="normal"]

[anim name=akane left="-=50" time=800 effect="easeInSine"]

#あかね
えー！　いいじゃん[l]

[cm]

#あかね
ところで、あおいちゃんは何やってるの？[l]

[cm]

[chara_mod name="aoi"  face="sad"]

#あおい
はぁ……、珍しい鳥がいたから見てたのよ[l]

[cm]

[chara_mod name="akane"  face="normal"]

[anim name=akane top="-=50" time=100 effect="easeInSine"]

[wa]

[anim name=akane top="+=50" time=100 effect="easeInSine"]

[wa]

#あかね
え！　どこどこ！？[l]

@layopt layer=message0 visible=false

[cm]

[chara_hide_all]

[bg storage="aosora.jpg" time="1500"]

[playse storage="suzume.ogg"]

[wait time=2000]

[bg storage="room.jpg" time="1500"]

[chara_mod name="akane"  face="happy"]

[chara_show name="aoi" left=50 top=80]

[chara_show name="akane" left=250 top=80]


@layopt layer=message0 visible=true

#あかね
あ！　そういえば、これから新しく出来たカフェ行くんだけど、一緒に行かない？[l]

[cm]

[chara_mod name="aoi"  face="sad"]

[anim name=aoi left="-=20" time=500 effect="easeInSine"]

[wa]

[anim name=aoi left="+=20" time=500 effect="easeInSine"]

[wa]

#あおい
もう少し鳥を見たいから遠慮しとくわ[l]

[cm]

#あかね
えー！　行こうよー[l]


[chara_mod name="aoi"  face="sad"]

[anim name=akane left="-=80" time=750 effect="easeInSine"]

[anim name=aoi left="-=80" time=750 effect="easeInSine"]

[wa]

[anim name=akane left="+=80" time=750 effect="easeInSine"]

[anim name=aoi left="+=80" time=750 effect="easeInSine"]

[wa]

[anim name=akane left="-=80" time=750 effect="easeInSine"]

[anim name=aoi left="-=80" time=750 effect="easeInSine"]

[wa]
[anim name=akane left="+=80" time=750 effect="easeInSine"]

[anim name=aoi left="+=80" time=750 effect="easeInSine"]

[wa]

[cm]

[chara_mod name="aoi"  face="angry"]

#あおい
ちょっと人前で恥ずかしいから辞めなさい！[l]

[cm]

#あおい
と言うより……いつまで抱き着いてるのよ！[l]

[playse storage="キック（強烈な打撃や衝撃）.ogg"]

[chara_mod name="akane"  face="doki"]

[anim name=akane left="+=250" time=500 effect="easeInSine"]

[wa]

@layopt layer=message0 visible=false

[mask]

[stopbgm]

[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
[mask_off]
[jump first.ks]

 